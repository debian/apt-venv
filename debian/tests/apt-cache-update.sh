#!/bin/sh

releases="oldstable stable testing unstable experimental xenial bionic focal jammy"

main(){
    for release in $releases; do
	apt-venv $release -u
    done
}

main || exit 77
